import { test } from '@japa/runner'

import {
  HeaderList,
  firstHeaderMatch
} from './index.js';

test.group('HeaderList', () => {
  test('parses and orders accept headers', ({ assert }) => {
    assert.deepEqual(
      [
        {
          value: 'test/1',
          weight: 1,
          parameters: {
            six: 'seven',
            eight: 'nine'
          }
        },
        {
          value: 'test/3',
          weight: 1,
          parameters: {
            one: 'two'
          }
        },
        {
          value: 'test/2',
          weight: 0.5,
          parameters: {}
        }
      ],
      (new HeaderList(
        'test/1;six=seven;eight = nine, test/2; q=0.5,test/3; one=two'
      )).list
    );
  });

  test('firstMatch() returns correct first match', ({ assert }) => {
    const list = new HeaderList(
      'test/1;six=seven;eight = nine;q=0.8, test/2; q=0.5 , test/3; one=two'
    );

    assert.deepEqual(list.firstMatch(['test/3', 'test/1']), {
      value: 'test/3',
      weight: 1,
      parameters: {
        one: 'two'
      }
    });
  });
});

test.group('firstHeaderMatch()', () => {
  test('returns the correct first match', ({ assert }) => {
    assert.deepEqual(firstHeaderMatch(
      'test/1;six=seven;eight = nine;q=0.8, test/2; q=0.5,test/3; one=two',
      ['test/3', 'test/1']
    ), {
      value: 'test/3',
      weight: 1,
      parameters: {
        one: 'two'
      }
    });
  });
});
