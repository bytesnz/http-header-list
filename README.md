# http-header-list 1.0.0

Parser and logic functions for HTTP header lists

[![pipeline status](https://gitlab.com/bytesnz/http-header-list/badges/main/pipeline.svg)](https://gitlab.com/bytesnz/http-header-list/-/commits/main)
[![http-header-list on NPM](https://bytes.nz/b/http-header-list/npm)](https://npmjs.com/package/http-header-list)
[![license](https://bytes.nz/b/http-header-list/custom?color=yellow&name=license&value=AGPL-3.0)](https://gitlab.com/bytesnz/http-header-list/-/blob/main/LICENSE)
[![developtment time](https://bytes.nz/b/http-header-list/custom?color=yellowgreen&name=development+time&value=~1+hours)](https://gitlab.com/bytesnz/http-header-list/-/blob/main/.tickings)
[![contributor covenant](https://bytes.nz/b/http-header-list/custom?color=purple&name=contributor+covenant&value=v1.4.1+adopted)](https://gitlab.com/bytesnz/http-header-list/-/blob/main/CODE_OF_CONDUCT.md)
[![support development](https://bytes.nz/b/http-header-list/custom?color=brightgreen&name=support+development&value=$$)](https://liberapay.com/MeldCE)

A dependency-free module to parse HTTP headers like `Accept` logic functions
to run common checks like finding the first matching value.

## Example

```js
/* eslint-disable no-console */
import { HeaderList } from 'http-header-list';

const acceptList = new HeaderList(
  'text/html, application/xhtml+xml, application/xml;q=0.9, */*;q=0.8'
);

const type = acceptList.firstMatch(['application/xml', 'application/xhtml+xml']);

console.log(type);

/* Output will be:
 * { value: 'application/xhtml+xml', weight: 1, parameters: {} }
 */

```

## Development

Feel free to post errors or feature requests to the project
[issue tracker](https://gitlab.com/bytesnz/http-header-list/issues) or
[email](mailto:contact-project+bytesnz-http-header-list-38124105-issue-@incoming.gitlab.com) them to us.
**Please submit security concerns as a
[confidential issue](https://gitlab.com/bytesnz/http-header-list/issues?issue[confidential]=true)**

The source is hosted on [Gitlab](https://gitlab.com/bytesnz/http-header-list) and uses
[eslint][], [prettier][], [lint-staged][] and [husky][] to keep things pretty.
As such, when you first [clone][git-clone] the repository, as well as
installing the npm dependencies, you will also need to install [husky][].

```bash
# Install NPM dependencies
npm install
# Set up husky Git hooks stored in .husky
npx husky install
```

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2022-07-27

Initial version!

[1.1.0]: https://gitlab.com/bytesnz/http-header-list/-/compare/v1.0.0...v1.1.0
[1.0.0]: https://gitlab.com/bytesnz/http-header-list/-/tree/v1.0.0


[husky]: https://typicode.github.io/husky
[eslint]: https://eslint.org/
[git-clone]: https://www.git-scm.com/docs/git-clone
[prettier]: https://prettier.io/
[lint-staged]: https://github.com/okonet/lint-staged#readme
[gitlab-ci]: https://docs.gitlab.com/ee/ci/
