/** @typedef {object} HeaderValue
 *
 * @property {string} value Option value
 * @property {number} weight Weight of option
 * @property {{[key: string]: string}} parameters Option parameters
 */
/** @constructor
 * Create a parse header object that contains the parsed header
 *
 * @param {string} header Header value to parse
 */
export function HeaderList(header: string): void;
export class HeaderList {
    /** @typedef {object} HeaderValue
     *
     * @property {string} value Option value
     * @property {number} weight Weight of option
     * @property {{[key: string]: string}} parameters Option parameters
     */
    /** @constructor
     * Create a parse header object that contains the parsed header
     *
     * @param {string} header Header value to parse
     */
    constructor(header: string);
    /**
     * The original header value
     */
    header: string;
    /** @type {HeaderValue[]}
     * The parsed and sorted list of header values. The header values are
     * sorted in descending order by the weight factor, q. If a header value
     * does not have a weight factor, it will default to 1
     */
    list: HeaderValue[];
    /**
     * Find the first matching value that matches one of the values in the
     * given array
     *
     * @param {string[]} possibleValues Possible values to match against
     */
    firstMatch(possibleValues: string[]): HeaderValue;
}
export function firstHeaderMatch(header: string, possibleValues: string[]): HeaderValue;
export type HeaderValue = {
    /**
     * Option value
     */
    value: string;
    /**
     * Weight of option
     */
    weight: number;
    /**
     * Option parameters
     */
    parameters: {
        [key: string]: string;
    };
};
