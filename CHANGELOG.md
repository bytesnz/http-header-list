# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2022-07-27

Initial version!

[1.1.0]: https://gitlab.com/bytesnz/http-header-list/-/compare/v1.0.0...v1.1.0
[1.0.0]: https://gitlab.com/bytesnz/http-header-list/-/tree/v1.0.0
