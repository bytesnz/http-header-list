/* eslint-disable no-console */
import { HeaderList } from 'http-header-list';

const acceptList = new HeaderList(
	'text/html, application/xhtml+xml, application/xml;q=0.9, */*;q=0.8'
);

const type = acceptList.firstMatch(['application/xml', 'application/xhtml+xml']);

console.log(type);

/* Output will be:
 * { value: 'application/xhtml+xml', weight: 1, parameters: {} }
 */
